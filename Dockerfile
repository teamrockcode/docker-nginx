# Dockerfile for nginx running on Ubuntu
##

# Pull base image.
FROM teamrock/ubuntu:production

# get updates
RUN apt-get -y update

# Install nginx
RUN apt-get install -y nginx

# Install php5-fpm
RUN apt-get install -y php5 php5-fpm php-pear php5-common php5-mcrypt php5-mysql php5-cli php5-gd php5-intl php5-curl php5-dev php5-apcu

# nginx shouldn't be run as a demon
ADD ./nginx.conf /etc/nginx/nginx.conf
ADD ./virtual-host.conf /etc/nginx/sites-enabled/default

# Expose ports.
EXPOSE 80

# Run nginx
ENTRYPOINT [ "php5-fpm && nginx" ]

